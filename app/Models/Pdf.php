<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pdf extends Model
{
    use HasFactory;

    protected $fillable = [
        'chapter_id',
        'title',
        'file_path',
    ];


    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }
}
