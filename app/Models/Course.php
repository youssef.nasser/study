<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'professor_id',
        'name',
    ];

    protected $append = [
        'count_chapters',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function getCountChaptersAttribute()
    {
        return $this->chapters->count();
    }

    public function professor()
    {
        return $this->belongsTo(Professor::class);
    }

    public function studySessions()
    {
        return $this->hasMany(StudySession::class);
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class);
    }

    public function createChapterWithPdf($chapter_name, $pdfFile)
    {
        $chapter = $this->chapters()->create([
            'name' => $chapter_name,
        ]);

        if ($pdfFile) {
            $chapter->storePdf($pdfFile);
        }

        return $chapter;
    }

    public function updateOrCreateChapters(array $chapterData, array $fileData)
    {
        foreach ($chapterData as $key => $chapterName) {
            $chapterId = isset($fileData['chapter_ids'][$key]) ? $fileData['chapter_ids'][$key] : null;
            $chapter = $this->chapters()->find($chapterId);

            if ($chapter) {
                $chapter->update(['name' => $chapterName]);
                if (isset($fileData['pdfs'][$key]) && $fileData['pdfs'][$key]->isValid()) {
                    $this->handleChapterPdf($chapter, $fileData['pdfs'][$key]);
                }
            } else {
                $chapter = $this->chapters()->create(['name' => $chapterName]);

                if (isset($fileData['pdfs'][$key]) && $fileData['pdfs'][$key]->isValid()) {
                    $this->handleChapterPdf($chapter, $fileData['pdfs'][$key]);
                }
            }
        }
    }

    public function deleteWithChapters()
    {
        $this->chapters->each(function ($chapter) {
            if ($chapter->pdf) {
                $this->deleteChapterPdf($chapter->pdf);
            }
        });

        $this->chapters()->delete();
        $this->delete();
    }

    private function deleteChapterPdf($pdf)
    {
        Storage::disk('public')->delete($pdf->file_path);
        $pdf->delete();
    }

    private function handleChapterPdf($chapter, $pdfFile)
    {
        if ($chapter->pdf) {
            Storage::disk('public')->delete($chapter->pdf->file_path);
            $chapter->pdf->delete();
        }

        $pdfPath = $pdfFile->store('pdfs', 'public');
        $chapter->pdf()->create([
            'title' => $pdfFile->getClientOriginalName(),
            'file_path' => $pdfPath,
        ]);
    }
}
