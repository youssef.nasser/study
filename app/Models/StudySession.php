<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudySession extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'title',
        'course_id',
        'date',
        'min_seconds',
        'min_minutes',
        'min_hours',
        'max_seconds',
        'max_minutes',
        'max_hours',
        'act_seconds',
        'act_minutes',
        'act_hours',
        'comment',
        'finished',
    ];
    protected $appends =
    [
        'status',
        'rate'
    ];

    public function getRateAttribute()
    {

        $max = $this->calculate($this->max_hours, $this->max_minutes, $this->max_seconds);
        $act = $this->calculate($this->act_hours, $this->act_minutes, $this->act_seconds);
        $min = $this->calculate($this->min_hours, $this->min_minutes, $this->min_seconds);

        if ($max - $min == 0) {
            $rate = (($act / $max) * 100);
            return number_format($rate, 2);
        }

        $rate =  100 + (($act / ($max - $min)) * 100) - 100;

        // Format $rate to display two decimal places
        $formattedRate = number_format($rate, 2);

        // You can return the formatted rate or use it as needed
        return $formattedRate;

    }

    public function calculate($hours,  $minutes,$seconds)
    {
        return $hours + ($minutes/60) + ($seconds/60/60);
    }

    public function getStatusAttribute()
    {
        if ($this->finished) {
            $status = "Completed";
        } elseif ($this->act_seconds || $this->act_hours || $this->act_minutes)
        {
            $status = "In Progress";
        }else{
            $status = "New";
        }
        return $status;
    }


    protected $casts = [
        'date' => 'date',
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function chapters()
    {
        return $this->belongsToMany(Chapter::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
