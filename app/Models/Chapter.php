<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Chapter extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_id',
        'name',
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function pdf()
    {
        return $this->hasOne(Pdf::class);
    }
    
    public function studySessions()
    {
        return $this->belongsToMany(StudySession::class);
    }

    public function storePdf($pdfFile)
    {
        $pdfPath = $pdfFile->store('pdf', 'public');

        return $this->pdf()->create([
            'title' => $pdfFile->getClientOriginalName(),
            'file_path' => $pdfPath,
        ]);
    }

    public function deletePdf()
    {
        Storage::delete($this->pdf->file_path);

        $this->pdf->delete();
    }
}
