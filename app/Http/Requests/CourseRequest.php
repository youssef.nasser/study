<?php

namespace App\Http\Requests;

use DB;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $userId = auth()->id(); // Assuming there's a logged-in user

        return [
            'name' => [
                'required',
                'string',
                Rule::unique('courses')->where(function ($query) use ($userId) {
                    $query->whereExists(function ($subquery) use ($userId) {
                        $subquery->select(DB::raw(1))
                            ->from('course_user')
                            ->where('user_id', $userId)
                            ->whereRaw('course_user.course_id = courses.id');
                    });
                }),
            ],
        ];
    }
}
