<?php

namespace App\Http\Controllers;

use App\Models\Pdf;
use App\Models\Course;
use App\Models\Chapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ChapterController extends Controller
{

    public function index()
    {
        $course = Course::find(request()->get('course_id'));
        $chapters = $course->chapters()->get();
        return view('chapters.index', [
            'chapters' => $chapters,
            'course' => $course,
        ]);
    }

    public function store(Request $request)
    {

        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $course = Course::find(request()->get('course_id'));
        $chapter = $course->chapters()->create($data);
        if ($request->hasFile('pdf')) {

            $chapter->storePdf($request->file('pdf'));
        }
            return redirect()->back()->with(['success' => 'Add successfully']);

    }

    public function update(Request $request, $chapter_id)
    {
        $data = $request->validate([
            'name' => 'required|string',
        ]);

        $chapter = Chapter::find($chapter_id);

        $chapter->update($data);

        if ($request->hasFile('pdf')) {
            if ($chapter->pdf) {
                Storage::disk('public')->delete($chapter->pdf->file_path);
                $chapter->pdf->delete();
                $chapter->storePdf($request->file('pdf'));
            } else {
                $chapter->storePdf($request->file('pdf'));
            }
        }

        return redirect()->back()->with(['success' => 'Edit successfully']);
    }




    public function storePdf(Request $request, $chapter_id)
    {
        Pdf::create([
            'chapter_id' => $chapter_id,
            'title' => $request->file('pdf')->getClientOriginalName(),
            'file_path' => $request->file('pdf')->store('pdf', 'public'),
        ]);

        return redirect()->back()->with(['success' => 'PDF uploaded successfully']);
    }

    public function destroy($chapter_id)
    {
        Chapter::find($chapter_id)->delete();
        return redirect()->back()->with(['success' => 'Deleted Success!']);
    }

    }

