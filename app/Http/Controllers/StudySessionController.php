<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Chapter;
use App\Models\StudySession;
use Illuminate\Http\Request;

class StudySessionController extends Controller
{
    public function index()
    {
        $sessions = auth()->user()->StudySessions()->get();

        return view('study_sessions.index', [
            'sessions' => $sessions,
        ]);
    }

    public function create()
    {
        $user = auth()->user();

        $professorsWithCourses = $user->professors->filter(function ($professor) {
            return $professor->course !== null;
        });

        $courses = $professorsWithCourses->map->course;

        return view('study_sessions.create', compact('courses'));
    }

    public function store(Request $request)
    {

        $data = $request->validate([
            'title' => 'required|string',
            'course_id' => 'required|exists:courses,id',
            'date' => 'required|date',
            'min_hours' => 'required|integer|min:0',
            'min_minutes' => 'required|integer|min:0|max:59',
            'min_seconds' => 'required|integer|min:0|max:59',
            'max_hours' => 'required|integer|min:0',
            'max_minutes' => 'required|integer|min:0|max:59',
            'max_seconds' => 'required|integer|min:0|max:59',
            'comment' => 'nullable|string',
            'chapters' => 'required|array',
        ]);
        $data['finished'] = false;
        $session = auth()->user()->studySessions()->create($data);

        $session->chapters()->attach($request->chapters);

        return redirect()->route('study_sessions.index')->with('success', 'Study session created successfully');
    }

    public function show($session_id)
    {

        $session = auth()->user()->studySessions()->find($session_id);

        return view('study_sessions.show',[
            'session' => $session,
            'chapters' => $session->chapters()->get(),
        ]);
    }

    public function record(Request $request , $session_id)
    {

        $timeArray = explode(':', $request->time);
        $session = auth()->user()->studySessions()->find($session_id);
        $session->update([
            'comment' => $request->comment,
            'finished' => (bool)$request->finished,
            'act_seconds'  =>  (int)$timeArray[2],
            'act_minutes'  =>  (int)$timeArray[1],
            'act_hours'    =>  (int)$timeArray[0],
        ]);
        return redirect()->route('study_sessions.index')->with('success', 'Study session ended successfully');
    }

    public function destroy($session_id)
    {
        $session = auth()->user()->studySessions()->find($session_id);
        $session->delete();
        return redirect()->back()->with('success', 'Study session deleted successfully.');
    }

    public function edit($sessionId)
    {
        $user = auth()->user();
        $session = $user->StudySessions()->find($sessionId);
        $selectedChapters = $session->chapters->pluck('id')->toArray();

        $professorsWithCourses = $user->professors->filter(function ($professor) {
            return $professor->course !== null;
        });

        $courses = $professorsWithCourses->map->course;
        $chapters = $professorsWithCourses->flatMap(function ($professor) {
            return $professor->course->chapters;
        });
        return view('study_sessions.edit', compact('session', 'courses', 'chapters', 'selectedChapters'));

    }

    public function update(Request $request, $session_id)
    {
        $data = $request->validate([
            'title' => 'required|string',
            'course_id' => 'required|exists:courses,id',
            'date' => 'required|date',
            'min_hours' => 'required|integer|min:0',
            'min_minutes' => 'required|integer|min:0|max:59',
            'min_seconds' => 'required|integer|min:0|max:59',
            'max_hours' => 'required|integer|min:0',
            'max_minutes' => 'required|integer|min:0|max:59',
            'max_seconds' => 'required|integer|min:0|max:59',
            'comment' => 'nullable|string',
            'chapters' => 'required|array',
        ]);

        $session = StudySession::find($session_id);
        $session->update($data);
        $session->chapters()->sync($data['chapters']);

        return redirect()->route('study_sessions.index')->with('success', 'Study session edited successfully');
    }

    public function getChapters(Request $request)
    {
        $course_id = $request->get('course_id');
        $course = Course::find($course_id);

        if (!$course) {
            return response()->json(['error' => 'Course not found'], 404);
        }

        return response()->json($course->chapters);
    }

}
