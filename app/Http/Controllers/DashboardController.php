<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $sessions = $user->studySessions()->get();

        $completedSessions = $sessions->where('finished', 1);

        $inProgressSessions = $user->studySessions()
        ->where(function ($query) {
            $query->where('act_hours', '>', 0)
                ->orWhere('act_minutes', '>', 0)
                ->orWhere('act_seconds', '>', 0);
        })
        ->where('finished', '=', 0)
        ->get();

        $new = $user->studySessions()
        ->where(function ($query) {
            $query
                ->whereNull('act_hours')
                ->orWhere('act_hours', '=', 0)
                ->whereNull('act_minutes')
                ->orWhere('act_minutes', '=', 0)
                ->whereNull('act_seconds')
                ->orWhere('act_seconds', '=', 0);
        })
        ->get();

        return view('dashboard', [
            'completedSessions' => $completedSessions,
            'inProgressSession' => $inProgressSessions,
            'new' => $new,
        ]);
    }
}
