<?php

namespace App\Http\Controllers;

use App\Models\Professor;
use Illuminate\Http\Request;

class ProfessorController extends Controller
{
    public function index()
    {
        return view('professors.index', [
            'professors' => auth()->user()->professors()->get(),
        ]);
    }

    public function create()
    {
        return view('professors.create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
        ]);
        auth()->user()->professors()->create($data);

        if ($request->from_course) {
            return redirect()->back()->with(['success' => 'Add Professor Success!']);
        } else {
            return redirect('/professors')->with(['success' => 'Add Success!']);
        }

    }

    public function destroy($professor_id)
    {
        Professor::find($professor_id)->delete();
        return to_route('professors.index')->with(['success' => 'Deleted Success!']);
    }

    public function edit()
    {
        return view('professors.edit', [
            'professor' => Professor::find(request()->get('professor_id')),
        ]);
    }

    public function update(Request $request, $professor_id)
    {
        $data = $request->validate([
            'name' => 'required',
        ]);

        Professor::find($professor_id)->update($data);

        return to_route('professors.index')->with(['success' => 'Update Success!']);
    }

    public function show($professor_id)
    {
        return view('professors.show',[
            'professor' => Professor::find($professor_id),
        ]);
    }
}
