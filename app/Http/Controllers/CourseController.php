<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Requests\CourseRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class CourseController extends Controller
{
    public function index()
    {
        $userProfessors = auth()->user()->professors;

        $courses = $userProfessors->map(function ($professor) {
            return $professor->course;
        })->filter();

        return view('courses.index', compact('courses'));
    }


    public function create()
    {
        return view('courses.create',[
            'professors' => auth()->user()->professors()->get(),
        ]);
    }

    public function store(CourseRequest $request)
    {
        // dd($request);

        $data = $request->validated();

        $data['professor_id'] = $request->professor_id;

        $course = Course::create($data);

        Auth::user()->courses()->attach($course->id);


        foreach ($request->chapters as $key => $chapter_name) {
            $course->createChapterWithPdf($chapter_name, $request->file('pdfs.' . $key));
        }

        return to_route('courses.index')->with( ['success' => 'Add Success!'] );
    }

    public function edit($course_id)
    {

        return view('courses.edit', [
            'course' => Course::find($course_id),
            'professors' => auth()->user()->professors()->doesntHave('course')->get(),
        ]);
    }

    public function update(Request $request, $course_id)
    {
        $data = $request->validate([
            'professor_id' => 'required',
            'name' => 'required',
        ]);
        
        $course = Course::find($course_id);

        $course->update($data);

        $course->updateOrCreateChapters($request->chapters, $request->all());

        return redirect()->route('courses.index')->with(['success' => 'Edit Success!']);
    }

    public function destroy($course_id)
    {
        $course = Course::find($course_id);

        if ($course) {
            $course->deleteWithChapters();
            return redirect()->route('courses.index')->with(['success' => 'Course and associated chapters deleted']);
        }

        return redirect()->route('courses.index')->with(['error' => 'Course not found']);
    }



}



