<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function signin(Request $request)
    {
        $data = $request->validate([
            'email'     => 'required|email',
            'password'  => 'required',
        ]);


        if(Auth::attempt($data)){
            session()->regenerate();
            return to_route('dashboard')->with(['success' => 'Login Success!']);

        }else{
            return back()->withErrors(['error' => 'Email or Password is wrong!']);
        }
    }

    public function register()
    {
        return view('auth.register');
    }

    public function signup(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        $data['password'] = bcrypt($data['password']);

        User::create($data);

        return redirect('login')->with(['success' => 'Register Success!']);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return to_route('login');
    }
}
