<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CourseController;

use App\Http\Controllers\ChapterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProfessorController;
use App\Http\Controllers\StudySessionController;
use League\CommonMark\Extension\SmartPunct\DashParser;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::group(['middleware' => 'guest'], function(){
    // Auth
    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/signin', [AuthController::class, 'signin'])->name('signin');
    Route::get('/register', [AuthController::class, 'register'])->name('register');
    Route::post('/signup', [AuthController::class, 'signup'])->name('signup');
});

Route::group(['middleware' => 'auth'], function(){
    // Dashboard
    Route::get('/', [DashboardController::class , 'index'])->name('dashboard');

    // Auth
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    // Professor
    Route::prefix('professors')->group(function () {
        Route::get('/', [ProfessorController::class, 'index'])->name('professors.index');
        Route::get('/create', [ProfessorController::class, 'create'])->name('professors.create');
        Route::post('/store', [ProfessorController::class, 'store'])->name('professors.store');
        Route::delete('/{professor_id}/destory', [ProfessorController::class, 'destroy'])->name('professors.destroy');
        Route::get('/edit', [ProfessorController::class, 'edit'])->name('professors.edit');
        Route::post('/{professor_id}/update', [ProfessorController::class, 'update'])->name('professors.update');
        Route::get('/{professor_id}/show', [ProfessorController::class, 'show'])->name('professors.show');
    });

    //Courses
    Route::prefix('courses')->group(function(){
        Route::get('/', [CourseController::class, 'index'])->name('courses.index');
        Route::get('/create', [CourseController::class, 'create'])->name('courses.create');
        Route::post('/store', [CourseController::class, 'store'])->name('courses.store');
        Route::get('/{course_id}/edit', [CourseController::class, 'edit'])->name('courses.edit');
        Route::post('/{course_id}/update', [CourseController::class, 'update'])->name('courses.update');
        Route::delete('/{course_id}/delete', [CourseController::class, 'destroy'])->name('courses.destroy');

    });

    //Chapters
    Route::prefix('chapters')->group(function(){
        Route::get('/', [ChapterController::class, 'index'])->name('chapters.index');
        Route::post('/store', [ChapterController::class, 'store'])->name('chapters.store');
        Route::get('/{chapter_id}/delete', [ChapterController::class, 'destroy'])->name('chapters.delete');
        Route::post('/{chapter_id}/storePdf', [ChapterController::class, 'storePdf'])->name('chapters.storePdf');
        Route::post('/{chapter_id}/update', [ChapterController::class, 'update'])->name('chapters.update');
    });

    Route::prefix('study-sessions')->group(function(){
        Route::get('/', [StudySessionController::class, 'index'])->name('study_sessions.index');
        Route::get('/create', [StudySessionController::class, 'create'])->name('study_sessions.create');
        Route::post('/store', [StudySessionController::class, 'store'])->name('study_sessions.store');
        Route::get('/{session_id}/show', [StudySessionController::class, 'show'])->name('study_sessions.show');
        Route::post('/{session_id}/record', [StudySessionController::class, 'record'])->name('study_sessions.record');
        Route::get('/{session_id}/edit', [StudySessionController::class, 'edit'])->name('study_sessions.edit');
        Route::post('/{session_id}/update', [StudySessionController::class, 'update'])->name('study_sessions.update');
        Route::get('/{session_id}/delete', [StudySessionController::class, 'destroy'])->name('study_sessions.delete');
        Route::get('/get-chapters', [StudySessionController::class, 'getChapters']);

    });
});


