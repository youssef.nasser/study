<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('study_sessions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('course_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->string('title');
            $table->dateTime('date')->unique();
            $table->text('comment')->nullable();
            $table->boolean('finished')->default(false);
            $table->unsignedInteger('min_seconds')->default(0);
            $table->unsignedInteger('min_minutes')->default(0);
            $table->unsignedInteger('min_hours')->default(0);
            $table->unsignedInteger('max_seconds')->default(0);
            $table->unsignedInteger('max_minutes')->default(0);
            $table->unsignedInteger('max_hours')->default(0);
            $table->unsignedInteger('act_seconds')->default(0);
            $table->unsignedInteger('act_minutes')->default(0);
            $table->unsignedInteger('act_hours')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('study_sessions');
    }
};
