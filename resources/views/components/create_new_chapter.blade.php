<script>
    function addChapter() {
        var chapterInputs = document.getElementById('chapterInputs');
        var newChapterDiv = document.createElement('div');
        newChapterDiv.innerHTML = `
            <div class="form-row" style="position: relative;">
                <div class="form-group" style="width: 45%;">
                    <label for="chapters">Chapter Name</label>
                    <input type="text" class="form-control" name="chapters[]" required  placeholder="Chapter Name">
                </div>
                <div class="form-group" style="width: 45%; margin-left: 10px">
                    <label for="pdfs">PDF</label>
                    <input type="file" class="form-control" name="pdfs[]">
                </div>
                <div class="form-group" style="position: absolute; bottom: 5px; right: 0;">
                    <button type="button" class="btn btn-danger" onclick="removeChapter(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </div>
        `;
        chapterInputs.appendChild(newChapterDiv);
    }

    function removeChapter(button) {
        var formGroup = button.parentNode;

        var chapterForm = formGroup.parentNode;

        chapterForm.parentNode.removeChild(chapterForm);
    }
</script>

