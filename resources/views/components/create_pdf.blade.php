
<!-- Modal -->
<div class="modal fade" id="addPdfModal" tabindex="-1" role="dialog" aria-labelledby="addPdfModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addPdfModalLabel">Add PDF Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Your PDF form goes here -->
                <form id="addPdfForm" method="POST" enctype="multipart/form-data">
                    @csrf
                    <!-- Include the chapter_id in a hidden input field -->
                    <input type="hidden" name="chapter_id" id="chapter_id">
                    <!-- Your form fields go here -->
                    <div class="form-group">
                        <label for="pdf">PDF File:</label>
                        <input type="file" name="pdf" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    // Function to show the add PDF modal
    function showAddPdfModal(chapterId) {
        // Set the chapter_id in the hidden input field
        document.getElementById('chapter_id').value = chapterId;

        // Set the action URL for the form dynamically
        var formAction = "{{ route('chapters.storePdf', ':chapterId') }}";
        formAction = formAction.replace(':chapterId', chapterId);

        // Set the form action attribute
        document.getElementById('addPdfForm').action = formAction;

        // Show the modal
        $('#addPdfModal').modal('show');
    }
</script>
