<script>
    function toggleFullscreen(chapterId) {
const iframe = document.getElementById(`v-tabs-${chapterId}`).querySelector('iframe');
if (iframe) {
if (iframe.requestFullscreen) {
    iframe.requestFullscreen();
} else if (iframe.mozRequestFullScreen) {
    iframe.mozRequestFullScreen();
} else if (iframe.webkitRequestFullscreen) {
    iframe.webkitRequestFullscreen();
} else if (iframe.msRequestFullscreen) {
    iframe.msRequestFullscreen();
}
}
}
// timer.jslet timerInterval;
let timerSeconds = {{ $session->act_seconds }};
let timerMinutes = {{ $session->act_minutes }};
let timerHours = {{ $session->act_hours }};

function startStopTimer(event) {
    event.preventDefault(); // Prevent the default form submission behavior

    const startStopBtn = document.getElementById('startStopBtn');

    if (startStopBtn.textContent === 'Start') {
        // Start the timer
        startStopBtn.textContent = 'Stop';
        timerInterval = setInterval(updateTimer, 1000);
    } else {
        // Stop the timer
        startStopBtn.textContent = 'Start';
        clearInterval(timerInterval);
    }
}

function endSession() {
    const timerDisplay = document.getElementById('timer');
    const completionCheckbox = document.getElementById('finishedCheckbox');
    const commentTextarea = document.querySelector('textarea[name="comment"]');
    const form = document.getElementById('endSessionForm');

    // Get the formatted time from the timer display
    const formattedTime = timerDisplay.textContent;

    // Set the completion status in a hidden input field
    const completionInput = document.createElement('input');
    completionInput.type = 'hidden';
    completionInput.name = 'finished';
    completionInput.value = completionCheckbox.checked ? '1' : '0';

    // Create a hidden input field for the time
    const timeInput = document.createElement('input');
    timeInput.type = 'hidden';
    timeInput.name = 'time';
    timeInput.value = formattedTime;

    // Append the hidden inputs to the form
    form.appendChild(completionInput);
    form.appendChild(timeInput);

    // Submit the form
    form.submit();
}
function updateTimer() {
    // Update the timer values
    timerSeconds++;

    if (timerSeconds > 59) {
        timerSeconds = 0;
        timerMinutes++;

        if (timerMinutes > 59) {
            timerMinutes = 0;
            timerHours++;
        }
    }

    // Display the updated timer
    const timerDisplay = document.getElementById('timer');
    timerDisplay.textContent = `${formatTime(timerHours)}:${formatTime(timerMinutes)}:${formatTime(timerSeconds)}`;
}

function formatTime(value) {
    // Helper function to add leading zeros
    return value < 10 ? `0${value}` : value;
}


</script>
