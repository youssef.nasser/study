@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="{{ asset('dist/css/card.css') }}">
@include('layouts.topbar', ['page_title' => 'Chapters For ' . $course->name, 'breadcrumb_item' => 'Show Course'])

<div class="content-wrapper">
    <div class="card">
        <div class="card-header">
            <div class="float-right">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createChapterModal">
                    <i class="fas fa-plus"></i> Create
                </button>
            </div>
        </div>
        @if(session('success'))
        <div class="alert alert-success mb-3">
            {{ session('success') }}
        </div>
    @endif

    <div class="card-body card-wrapper">
        @foreach ($chapters as $chapter)
            <div class="max-w-sm p-6 custom-card bg-blue-500 border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                <div class="embed-responsive embed-responsive-16by9" style="height: 320px">
                    @if($chapter->pdf)
                        <iframe class="embed-responsive-item" src="{{ asset('storage/' . $chapter->pdf->file_path) }}" allowfullscreen></iframe>
                    @else
                        <img class="embed-responsive-item" src="{{ asset('storage/pdf/pdfimage.jpg') }}" alt="PDF Image">
                    @endif
                </div>

                <br>


                <p class="mb-3 font-normal text-black dark:text-gray-400">
                    <span style="font-weight: bold">
                        {{$chapter->name}}
                    </span>
                    with Professor
                    <a href="{{ route('professors.show', $course->professor->id)}}">
                        {{$course->professor->name}}
                    </a>
                </p>

                <div class="mb-3 d-flex mt-2">
                    @if($chapter->pdf)
                        <a href="{{ asset('storage/' . $chapter->pdf->file_path) }}" class="btn btn-primary mr-2" target="_blank">View pdf</a>
                    @else
                        <button class="btn btn-primary mr-2" onclick="showAddPdfModal({{ $chapter->id }})">Add pdf</button>
                    @endif
                    <button type="button" class="btn btn-warning mr-2 edit-chapter-btn"
                        data-toggle="modal" data-target="#editChapterModal"
                        data-chapter-name="{{ $chapter->name }}" data-chapter-id="{{ $chapter->id }}"
                    >Edit</button>
                    <button class="btn btn-danger" onclick="window.location ='{{ route('chapters.delete', $chapter) }}'">Delete</button>
                </div>

            </div>
        @endforeach
    </div>


</div>
@extends('components.create_pdf')



<div class="modal fade" id="editChapterModal" tabindex="-1" role="dialog" aria-labelledby="editChapterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editChapterModalLabel">Edit Chapter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('chapters.update', $chapter->id) }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Chapter Name</label>
                        <input type="text" class="form-control" name="name" id="edit-chapter-name" placeholder="Enter Chapter Name" required>
                    </div>
                    <div class="form-group">
                        <label for="pdf">PDF File</label>
                        <input type="file" class="form-control-file" id="pdf" name="pdf">
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createChapterModal" tabindex="-1" role="dialog" aria-labelledby="createChapterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createChapterModalLabel">Create Chapter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form  action="{{ route('chapters.store', ['course_id' =>   $course->id] ) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Chapter Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter Chapter Name" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="pdf">PDF File</label>
                        <input type="file" class="form-control-file" id="pdf" name="pdf">
                    </div>
                    <button type="submit" class="btn btn-primary">Update Chapter</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('.edit-chapter-btn').click(function () {
        // Get the chapter information from the button's data attributes
        var chapterName = $(this).data('chapter-name');
        var chapterId = $(this).data('chapter-id');

        // Set the values in the modal form
        $('#edit-chapter-name').val(chapterName);

        // Update the form action URL to include the chapter ID
        var formAction = "{{ route('chapters.update', ':chapterId') }}";
        formAction = formAction.replace(':chapterId', chapterId);
        $('#editChapterModal form').attr('action', formAction);
    });
</script>

@endsection

