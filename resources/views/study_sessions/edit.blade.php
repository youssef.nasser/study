@extends('layouts.app')

@section('content')
    @include('layouts.topbar', ['page_title' => 'Edit Study Sessions', 'breadcrumb_item' => 'Edit Study Sessions'])
    <link rel="stylesheet" href="{{ asset('dist/css/study.css') }}">
    <div class="content-wrapper">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Study Session</h3>
            </div>

            <form action="{{ route('study_sessions.update', $session->id ) }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{$session->title}}">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="course_id">Course</label>
                            <select class="form-control" id="course_id" name="course_id" onchange="loadChapters(this.value)">
                                @foreach ($courses as $course)
                                    <option value="{{ $course->id }}">{{ $course->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="chapter_id">Chapters</label>
                            <select class="form-control" id="chapter_id" name="chapters[]" multiple>
                                <option value="">Select chapters</option>
                                @foreach ($chapters as $chapter)
                                    <option value="{{ $chapter->id }}" {{ in_array($chapter->id, $selectedChapters) ? 'selected' : '' }}>
                                        {{ $chapter->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                                        <div class="form-group">
                        <label for="date">Date</label>
                        <div class="input-group">
                        </div>
                        <input type="datetime-local" id="date" name="date"  value="{{ $session->date }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <div class="input-group row">
                            <div class="col-4">
                                <label for="min_hours">Minimum Hours</label>
                                <input type="number" id="min_hours" name="min_hours" min="0" value="{{ $session->min_hours }}">
                            </div>

                            <div class="col-4">
                                <label for="min_minutes">Minimum Minutes</label>
                                <input type="number" id="min_minutes" name="min_minutes" min="0" max="59" value="{{ $session->min_minutes }}">
                            </div>

                            <div class="col-4">
                                <label for="min_seconds">Minimum Seconds</label>
                                <input type="number" id="min_seconds" name="min_seconds" min="0" max="59" value="{{ $session->min_seconds }}">
                            </div>
                        </div>
                        <br>
                        <div class="input-group row">
                            <div class="col-4">
                                <label for="max_hours">Maximum Hours</label>
                                <input type="number" id="max_hours" name="max_hours" min="0" value="{{ $session->max_hours }}">
                            </div>

                            <div class="col-4">
                                <label for="max_minutes">Maximum Minutes</label>
                                <input type="number" id="max_minutes" name="max_minutes" min="0" max="59" value="{{ $session->max_minutes }}">
                            </div>

                            <div class="col-4">
                                <label for="max_seconds">Maximum Seconds</label>
                                <input type="number" id="max_seconds" name="max_seconds" min="0" max="59" value="{{ $session->max_seconds }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mix_study_time" >Comment</label>
                        <div class="input-group">
                            <textarea name="comment" id="comment" cols="170" rows="3" style="border: 1px solid #ccc; border-radius: 5px;">{{ $session->comment }}</textarea>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        document.getElementById('course_id').addEventListener('change', function(e) {
            var course_id = e.target.value;

            var xhr = new XMLHttpRequest();
            xhr.open('GET', '/study-sessions/get-chapters?course_id=' + course_id, true);
            xhr.onload = function() {
                if (this.status == 200) {
                    var chapters = JSON.parse(this.responseText);
                    var chapterSelect = document.getElementById('chapter_id');

                    while (chapterSelect.firstChild) {
                        chapterSelect.removeChild(chapterSelect.firstChild);
                    }

                    chapters.forEach(function(chapter) {
                        var option = document.createElement('option');
                        option.value = chapter.id;
                        option.text = chapter.name;
                        chapterSelect.appendChild(option);
                    });
                }
            };
            xhr.send();
        });
    </script>
@endsection

