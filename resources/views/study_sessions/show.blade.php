@extends('layouts.app')

@section('content')
    @include('layouts.topbar', ['page_title' => ' Show Session ', 'breadcrumb_item' => 'Study Sessions'])

    <link rel="stylesheet" href="{{ asset('dist/css/show.session.css') }}">

    <!-- Add these lines to include Bootstrap CSS from CDN -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Add these lines to include Bootstrap JavaScript (bundle includes Popper.js) from CDN -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

    <div class="content-wrapper">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class="card card-primary card-outline">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box">
                            <div class="info-box-content">
                                <span class="info-box-text">Title</span>
                                <span class="info-box-number">{{ $session->title }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box">
                            <div class="info-box-content">
                                <span class="info-box-text">Appointment</span>
                                <span class="info-box-number">{{$session->date->format('Y-m-d') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box">
                            <div class="info-box-content">
                                <span class="info-box-text">Min</span>
                                <span class="info-box-number">{{ $session->min_hours . ':' . $session->min_seconds . ':' . $session->min_minutes}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box">
                            <div class="info-box-content">
                                <span class="info-box-text">Max</span>
                                <span class="info-box-number">{{ $session->max_hours . ':' . $session->max_seconds . ':' . $session->max_minutes}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-12">
                        <form id="endSessionForm" action="{{ route('study_sessions.record', $session->id) }}" method="POST">
                            @csrf
                            <div class="info-box" style="height: 120px">
                                <div class="info-box-content">
                                    <span class="info-box-text">Timer</span>
                                    <span class="info-box-number" id="timer">
                                        {{$session->act_hours}}:{{$session->act_minutes}}:{{$session->act_seconds}}
                                    </span>
                                    <button class="btn btn-success" id="startStopBtn" onclick="startStopTimer(event)">Start</button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="info-box" style="height: 120px">
                                <div class="info-box-content">
                                    <span class="info-box-text">Comment</span>
                                    <textarea name="comment" rows="2" cols="50" placeholder="Additional Comments"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box" style="height: 120px">
                                <div class="info-box-content">
                                    <div>
                                        <div>
                                            <input type="checkbox" name="finished" id="finishedCheckbox">
                                            <label for="finished">finished</label>
                                        </div>
                                        <button class="btn btn-success" style="width: 250px" type="button" onclick="endSession()">End</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

                <div class="row w-100">
                    <div class="col-9">
                        <div class="tab-content" id="v-tabs-tabContent">
                            @foreach ($chapters as $chapter)
                                <div class="tab-pane fade{{ $loop->first ? ' show active' : '' }}"
                                    id="v-tabs-{{ $chapter->id }}" role="tabpanel"
                                    aria-labelledby="v-tabs-{{ $chapter->id }}-tab">
                                    <div class="embed-responsive embed-responsive-16by9" style="height: 1000px">
                                        @if($chapter->pdf)
                                            <iframe class="embed-responsive-item" src="{{ asset('storage/' . $chapter->pdf->file_path) }}" allowfullscreen></iframe>
                                        @else
                                            <img class="embed-responsive-item" src="{{ asset('storage/pdf/pdfimage.jpg') }}" alt="PDF Image">
                                        @endif
                                    </div>
                                    <div class="col-12 mt-2 text-left">
                                        <button class="btn btn-primary btn-sm expand-btn" onclick="toggleFullscreen('{{ $chapter->id }}')">
                                            <i class="fas fa-expand-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-3 ml-auto chapter-card">
                        <div class="nav flex-column nav-tabs text-center" id="v-tabs-tab" role="tablist" aria-orientation="vertical">
                            @foreach ($chapters as $chapter)
                                <a data-bs-toggle="pill" class="nav-link{{ $loop->first ? ' active' : '' }}"
                                    id="v-tabs-{{ $chapter->id }}-tab" href="#v-tabs-{{ $chapter->id }}" role="tab"
                                    aria-controls="v-tabs-{{ $chapter->id }}" aria-selected="{{ $loop->first }}"
                                    style="font-weight: bold; color: blue;">
                                    {{ $chapter->name }}
                                </a>
                            @endforeach
                        </div>

                    </div>

                </div>
            </div>
        </div>
        @extends('components.record_study_session')

@endsection
