@extends('layouts.app')
@section('content')
@include('layouts.topbar', ['page_title' => 'Study Sessions', 'breadcrumb_item' => 'Study Sessions'])

<link rel="stylesheet" href="{{ asset('dist/css/session.css') }}">

<div class="content-wrapper" style="height: 1000px;">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <div class="card-header">
        <div class="float-right">
            <button type="button" class="btn btn-success" onclick="window.location ='{{ route('study_sessions.create') }}'">
                <i class="fas fa-plus"></i> Create
            </button>
        </div>
    </div>

    <div class="card-body card-wrapper">
        @foreach ($sessions as $session)
            <div style="width: 30%; font-size: 18px" class="max-w-sm p-6 custom-card bg-blue-500 border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                <h4 class="text-center">
                    <p class="mb-3 font-normal text-black dark:text-gray-400">{{ $session->title }}</p>
                </h4>
                <hr>
                <hr>

                <span style="font-size: 20px;font-weight: bold;">
                    Course :
                </span>
                <a href="{{ route('chapters.index', ['course_id' =>  $session->course->id]) }}">
                    {{ $session->course->name }}
                </a>
                <br>


                <span style="font-size: 20px;font-weight: bold;">
                    Appointment :
                </span>
                {{ $session->date->format('Y-m-d') }}
                <br>

                <span style="font-size: 20px;font-weight: bold;">
                    Status :
                </span>
                @if($session->status == 'Completed')
                    <span style="color: green;font-weight: bold;">{{ $session->status }} </span>
                @elseif($session->status == 'New')
                    <span style="color: rgb(0, 106, 255);font-weight: bold;">{{ $session->status }} </span>
                @elseif($session->status == 'In Progress')
                    <span style="color: rgb(255, 162, 0);font-weight: bold;">{{ $session->status }} </span>
                @endif
                <br>
                <h3 class="rate-label">Rate: {{ $session->rate }}<sup>%</sup></h3>

                <br>
                <details>
                    <summary><span style="font-size: 20px;font-weight: bold;">Chapters </span></summary>

                    <table class="table border table-sm table-striped mt-2">
                        @foreach ($session->chapters as $key => $chapter)
                            <tr>
                                <th class="w-2">{{ $key + 1 . "- "}}</th>
                                <td>
                                    @if($chapter->pdf)
                                        {{ $chapter->name }}
                                        <br>
                                        <a href="{{ asset('storage/' . $chapter->pdf->file_path) }}" target="_blank">View pdf</a>
                                    @else
                                        {{ $chapter->name }}
                                        <br>
                                        <button  class="btn btn-primary mr-2" onclick="showAddPdfModal({{ $chapter->id }})">Add pdf</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </details>
                <br>
                <div class="d-flex justify-content-between align-items-start">

                    <a href="{{ route('study_sessions.show', $session->id) }}" title="Go to next">
                        <div class="shadow__btn" style="width: 62px">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                <path d="M5 12h14M12 5l7 7-7 7"></path>
                            </svg>
                        </div>
                    </a>
                    <button class="btn btn-warning" style="width: 80px; height:40px" onclick="window.location.href='{{ route('study_sessions.edit', $session->id) }}'">Edit</button>

                    <button class="btn btn-danger"  style="width: 80px; height:40px" onclick="confirmDelete('{{ route('study_sessions.delete', $session->id) }}')">Delete</button>
                </div>
            </div>
            @endforeach
    </div>
</div>
@extends('components.create_pdf')

<script>
    function confirmDelete(deleteUrl) {
        var result = confirm("Are you sure you want to delete the session?");
        if (result) {
            window.location = deleteUrl;
        }
    }
</script>
@endsection
