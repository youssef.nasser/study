@extends('layouts.app')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Professors Table</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Professors Table</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="float-right">
                                <button type="button" class="btn btn-success" onclick="window.location ='{{route('professors.create')}}'">
                                    <i class="fas fa-plus"></i> Create
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 5px">#</th>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Course</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($professors as $key => $professor)
                                        <tr>
                                            <td class="text-center">{{ $key + 1  }}</td>
                                            <td class="text-center">{{ $professor->id }}</td>
                                            <td class="text-center">{{ $professor->name }}</td>
                                            <td class="text-center">
                                                @if ($professor->course)
                                                    <a href="{{ route('chapters.index', ['course_id' =>  $professor->course->id] ) }}">{{ $professor->course->name }}</a>
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                                <td class="text-center">

                                            <a  href="{{ route('professors.edit', ['professor_id' => $professor->id ] ) }}" class="btn btn-warning">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>

                                                <form action="{{ route('professors.destroy', $professor->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this professor?')" class="d-inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
