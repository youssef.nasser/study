@extends('layouts.app')

@section('content')
    @include('layouts.topbar', ['page_title' => 'Professor ' . $professor->name, 'breadcrumb_item' => 'Show Professor'])
    <link rel="stylesheet" href="{{ asset('dist/css/professor.css') }}">

    <div class="content-wrapper">
        <div class="relative flex justify-center items-center w-80 flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md">
            <i class="fas fa-user-tie text-9xl mb-4"></i>
            <div class="p-6">
                <h5 class="mb-2 block font-sans text-xl font-semibold leading-snug tracking-normal text-blue-gray-900 antialiased">
                    {{$professor->name}} :
                </h5>
                <p class="block font-sans text-base font-light leading-relaxed text-inherit antialiased">
                    {{$professor->name}} is a professor for {{$professor->course->name}} course.
                </p>
            </div>
            <div class="p-6 pt-0">
                <a href="{{ route('chapters.index', ['course_id' =>  $professor->course->id]) }}" class="btn btn-primary" target="_blank">View Course</a>
            </div>
        </div>
    </div>
@endsection
