@extends('layouts.app')
@section('content')
    @include('layouts.topbar', ['page_title' => 'Add Course' , 'breadcrumb_item' => 'Add Course'])

    <div class="content-wrapper">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Course</h3>
                    </div>
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                    <form action="{{ route('courses.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <label for="professor_id">Professor</label>
                            <div class="form-group row">
                                <div class="col-sm-9">
                                    <select class="form-control" id="professor_id" name="professor_id" required>
                                        <option value="">Select...</option>
                                        @foreach ($professors as $professor)
                                            @php
                                                $hasCourse = $professor->course ? true : false;
                                            @endphp
                                            <option value="{{ $professor->id }}" {{ $hasCourse ? 'disabled' : '' }}>
                                                {{ $professor->name }}{{ $hasCourse ? ' (Already has a course)' : '' }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProfessorModal">Add Professor</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required>
                            </div>
                            <hr>
                            <div id="chapterInputs">
                                <div class="form-group">
                                    <label for="chapters">
                                        Chapter Name
                                    </label>
                                    <input type="text" class="form-control" id="chapters" name="chapters[]" required placeholder="Chapter Name">
                                </div>
                                <div class="form-group">
                                    <label for="pdfs">PDF</label>
                                    <input type="file" class="form-control" id="pdfs" name="pdfs[]">
                                </div>
                            </div>
                            <button type="button" class="btn btn-success" onclick="addChapter()">Add Chapter</button>
                        </div>

                        @extends('components.create_professor')
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @extends('components.create_new_chapter')
@endsection
