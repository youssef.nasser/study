@extends('layouts.app')
@section('content')
    @include('layouts.topbar', ['page_title' => 'edit course', 'breadcrumb_item' => 'edit course'])

    <div class="content-wrapper">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Course</h3>
                    </div>

                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <form action="{{ route('courses.update', $course->id ) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('POST')

                        <div class="card-body">
                            <label for="professor_id">Professor</label>
                            <div class="form-group row">
                                <div class="col-sm-9">
                                    <select class="form-control" id="professor_id" name="professor_id">
                                        <option value="{{ $course->professor->id }}">{{$course->professor->name}}</option>
                                        @foreach ($professors as $professor)
                                            <option value="{{ $professor->id }}" {{ $professor->id == $course->professor_id ? 'selected' : '' }}>{{ $professor->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProfessorModal">Add Professor</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="{{ $course->name }}" required>
                            </div>

                            <hr>

                            <div id="chapterInputs">
                                @foreach ($course->chapters as $chapter)
                                <div class="form-row" style="position: relative;">
                                    <div class="form-group" style="width: 45%;">
                                        <label for="chapters">Chapter Name</label>
                                        <input type="text" class="form-control" id="chapters" name="chapters[]" value="{{ $chapter->name }}" required>
                                    </div>

                                    <div class="form-group" style="width: 45%; margin-left: 10px;">
                                        <label for="pdfs">New PDF</label>
                                        <input type="file" class="form-control" id="pdfs" name="pdfs[]">
                                    </div>
                                    <input type="hidden" name="chapter_ids[]" value="{{ $chapter->id }}">

                                    <div class="form-group" style="position: absolute; bottom: 5px; right: 0;">
                                        <button type="button" class="btn btn-danger ml-2" onclick="confirmDeleteChapter({{ $chapter->id }})">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </div>

                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <p class="text-muted">
                                            Existing PDF:
                                            @if($chapter->pdf)
                                                <a href="{{ asset('storage/' . $chapter->pdf->file_path) }}" target="_blank">View PDF</a>
                                            @else
                                                No PDF attached
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            @endforeach


                            </div>

                        </div>

                        <div class="card-footer">
                            <button type="button" class="btn btn-success" onclick="addChapter()">Add Another chapter</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @extends('components.create_professor')
    @extends('components.create_new_chapter')
@endsection

<script>
    function confirmDeleteChapter(chapterId) {
        if (confirm('Are you sure you want to delete this chapter?')) {
            // Redirect to the delete route using the route helper
            window.location.href = '{{ route('chapters.delete', ['chapter_id' => '__chapterId__']) }}'.replace('__chapterId__', chapterId);
        }
    }
</script>
