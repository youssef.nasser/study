@extends('layouts.app')
@section('content')
@include('layouts.topbar', ['page_title' => 'Courses Table', 'breadcrumb_item' => 'Courses Tables'])
<div class="content-wrapper">
    @if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                                <div class="float-right">
                                <button type="button" class="btn btn-success" onclick="window.location ='{{ route('courses.create') }}'">
                                    <i class="fas fa-plus"></i> Create
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Professor</th>
                                        <th class="text-center">Chapters</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($courses as $key => $course)
                                        <tr>
                                            <td class="text-center">{{$key + 1 }}</td>
                                            <td class="text-center">{{$course->id}}</td>
                                            <td class="text-center">{{ $course->name }}</td>
                                            <td class="text-center"><a href="{{ route('professors.show', $course->professor->id ) }}">{{$course->professor->name}}</a></td>
                                            <td class="text-center"><a href="{{ route('chapters.index', ['course_id' =>  $course->id]   ) }}">{{$course->count_chapters}}</a></td>
                                            <td class="text-center">


                                                <a href="{{ route('chapters.index', ['course_id' =>  $course->id]) }}" type="submit" class="btn btn-success" title="show chapters" >
                                                    <i class="fas fa-book"></i>
                                                </a>

                                                <a href="{{ route('courses.edit', ['course_id' =>  $course->id]) }}" type="submit" class="btn btn-warning" title="show chapters" >
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>

                                                <form action="{{ route('courses.destroy', $course->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this professor?')" class="d-inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger" title="delete course">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
