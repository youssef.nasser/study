
<link rel="stylesheet" href="{{ asset('dist/css/logout.css') }}">

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <span class="brand-text font-weight-light">Study</span>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column">
                <li class="nav-item menu-open mb-3">
                    <a href="{{ route('dashboard') }}" class="nav-link active" style="background-color: #1d447e; color: #ffffff;">
                        <i class="fas fa-tachometer-alt"></i> <!-- Dashboard icon -->
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item mb-3">
                    <a href="{{ route('courses.index') }}" class="nav-link active bg-info">
                        <i class="fas fa-book-open"></i>
                        <p>Courses</p>
                    </a>
                </li>

                <li class="nav-item mb-3">
                    <a href="{{ route('professors.index') }}" class="nav-link active bg-success">
                        <i class="fas fa-user-tie"></i>
                        <p>Professors</p>
                    </a>
                </li>

                <li class="nav-item mb-3">
                    <a href="{{ route('study_sessions.index') }}" class="nav-link active bg-danger">
                        <i class="fas fa-graduation-cap"></i>
                        <p>Study Sessions</p>
                    </a>
                </li>
            </ul>
        </nav>
        <div>
            <a class="fixed-bottom text-left cool-logout-button" href="{{ route('logout') }}">
                <i class="fas fa-sign-out-alt"></i> 
            </a>
        </div>
    </div>
</aside>
